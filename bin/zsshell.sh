#!/bin/bash

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

if [ -z "$2" ]; then
    echo
    echo "USAGE: $0 <instance_hostname> <ddd_domain>"
    echo
    echo "  example:"
    echo "    $0 dev.zaaksysteem.nl case_management"
    echo
    exit
fi

ipython -i -m zsnl_shell $1 $2