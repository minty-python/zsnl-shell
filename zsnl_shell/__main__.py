# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import sys
from . import ZSShell

zss = ZSShell()

if sys.argv[1]:
    zss.domain(sys.argv[1])

if sys.argv[2]:
    zss.ddd(sys.argv[2])
