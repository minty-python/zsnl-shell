# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

__version__ = "0.0.1"

from minty import Base
from minty.cqrs import CQRS
from minty.infrastructure import InfrastructureFactory
from minty.middleware import AmqpPublisherMiddleware
from minty_infra_sqlalchemy import DatabaseTransactionMiddleware
from uuid import uuid4
from zsnl_domains import admin, case_management, document

EXISTING_DOMAINS = {
    "case_management": case_management,
    "document": document,
    # "communication": communication,
    "admin": admin,
}


class ZSShell(Base):
    cqrs = None
    infra_factory = None
    ddd_name = None
    fqdn = None
    config = None
    cmd = None
    qry = None

    def __init__(self):
        self.infra_factory = InfrastructureFactory(config_file="config.conf")

    ### Preperations

    def ddd(self, name):
        self._assert_domain()
        if name not in EXISTING_DOMAINS.keys():
            raise ("DDD Domain does not exist")

        self.ddd_name = name
        self.cqrs = CQRS(
            domains=[EXISTING_DOMAINS[name]],
            infrastructure_factory=self.infra_factory,
            command_wrapper_middleware=[
                DatabaseTransactionMiddleware("database"),
                AmqpPublisherMiddleware(
                    publisher_name=name, infrastructure_name="amqp"
                ),
            ],
        )

        self._load_commands_and_queries()

    def domain(self, name):
        self.fqdn = name

    ### Usage

    def repo(self, name):
        self._assert_requirements()
        return self.cqrs

    ### Private methods
    def _assert_ddd_domain(self):
        if not self.ddd_name:
            raise Exception(
                "Cannot run action, please select ddd domain first by running zss.ddd(name)"
            )

        return True

    def _assert_domain(self):
        if not self.fqdn:
            raise Exception(
                "Cannot run action, please select domainname first by running zss.domain(host)"
            )

        return True

    def _assert_requirements(self):
        self._assert_domain()
        self._assert_ddd_domain()

    def _load_commands_and_queries(self):
        self.cmd = self.cqrs.get_command_instance(
            str(uuid4), "zsnl_domains." + self.ddd_name, self.fqdn, None,
        )
        self.qry = self.cqrs.get_query_instance(
            str(uuid4), "zsnl_domains." + self.ddd_name, self.fqdn, None,
        )
